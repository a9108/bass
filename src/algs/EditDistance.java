package algs;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedList;

import org.apache.commons.collections.functors.ForClosure;

import basic.Config;
import basic.algorithm.StringAlg;
import basic.format.Pair;

public class EditDistance extends Model {

	private HashMap<Integer, LinkedList<Pair<Integer, Double>>> distance;
	private int MAXLIST = 100;

	public EditDistance(int THRES) {
		this.THRES = THRES;
		modelName = "EditDistance (Thres=" + THRES + ")";
	}

	@Override
	public void run() {
		distance = new HashMap<Integer, LinkedList<Pair<Integer, Double>>>();
		for (int i = 0; i < data.getSizeA(); i++) {
			LinkedList<Pair<Integer, Double>> cur = new LinkedList<Pair<Integer, Double>>();
			String nameA = data.getNameA(i);

			final LinkedList<Integer> Q = new LinkedList<Integer>();
			for (int j = 0; j < data.getSizeB(); j++)
				Q.add(j);
			Thread[] workers = new Thread[Integer.valueOf(Config
					.getValue("Threads"))];
			for (int q = 0; q < workers.length; q++) {
				workers[q] = new Thread() {
					@Override
					public void run() {
						for (;;) {
							int id;
							synchronized (Q) {
								if (Q.isEmpty())
									return;
								id = Q.removeFirst();
							}
							String nameB = data.getNameB(id);
							double dist = 0.0 + StringAlg.EditDistance(nameA,
									nameB);
							synchronized (cur) {
								cur.add(new Pair<Integer, Double>(id, dist));
							}
						}
					}
				};
				workers[q].start();
			}
			for (Thread worker : workers)
				try {
					worker.join();
				} catch (Exception e) {
				}

			Collections.sort(cur, new Comparator<Pair<Integer, Double>>() {
				@Override
				public int compare(Pair<Integer, Double> a,
						Pair<Integer, Double> b) {
					if (a.getSecond() != b.getSecond())
						return a.getSecond().compareTo(b.getSecond());
					return a.getFirst().compareTo(b.getFirst());
				}
			});
			for (; cur.size() > MAXLIST; cur.removeLast())
				;
			distance.put(i, cur);
		}
	}

	private double THRES = 2;

	@Override
	public LinkedList<Pair<Integer, Integer>> getPairs() {
		System.out.println("EditDistance Threshold = " + THRES);
		LinkedList<Pair<Integer, Integer>> res = new LinkedList<Pair<Integer, Integer>>();
		for (int i : distance.keySet()) {
			for (Pair<Integer, Double> link : distance.get(i))
				if (link.getSecond() <= THRES)
					res.add(new Pair<Integer, Integer>(i, link.getFirst()));
		}
		return res;
	}

	@Override
	public HashMap<Integer, Integer> getMapping() {
		HashMap<Integer, Integer> res = new HashMap<Integer, Integer>();
		System.out.println("EditDistance Threshold = " + THRES);
		for (int i : distance.keySet()) {
			if (distance.get(i).getFirst().getSecond() <= THRES)
				res.put(i, distance.get(i).getFirst().getFirst());
			else
				res.put(i, -1);
		}
		return res;
	}

	@Override
	public HashMap<Integer, ArrayList<Integer>> getList() {
		HashMap<Integer, ArrayList<Integer>> res = new HashMap<Integer, ArrayList<Integer>>();
		for (int i : distance.keySet()) {
			ArrayList<Integer> cur = new ArrayList<Integer>();
			for (Pair<Integer, Double> link : distance.get(i))
				cur.add(link.getFirst());
			res.put(i, cur);
		}
		return res;
	}
}
