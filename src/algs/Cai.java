package algs;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Random;

import basic.Config;
import basic.Functions;
import basic.Vector;
import basic.algorithm.StringAlg;
import basic.format.Pair;

public class Cai extends Model {

	private int K = 100;
	private double lambda = 1e-2;
	private double alpha = 1e-2;
	private double[][] embed;
	private HashMap<Integer, Integer> idA, idB;

	private HashMap<Integer, ArrayList<Integer>> res;
	private int MAXLIST = 10000;

	private Random random;

	private class TrainData {
		int site, i, j, v;

		public TrainData(int site, int i, int j, int v) {
			this.site = site;
			this.i = i;
			this.j = j;
			this.v = v;
		}
	}

	private LinkedList<TrainData> train;

	@Override
	public void run() {
		random = new Random();
		idA = new HashMap<Integer, Integer>();
		idB = new HashMap<Integer, Integer>();
		int cnt = 0;
		for (int i : data.getTrain().keySet()) {
			idA.put(i, cnt);
			idB.put(data.getTrain().get(i), cnt++);
		}
		for (int i = 0; i < data.getSizeA(); i++)
			if (!idA.containsKey(i))
				idA.put(i, cnt++);
		for (int i = 0; i < data.getSizeB(); i++)
			if (!idB.containsKey(i))
				idB.put(i, cnt++);

		embed = new double[cnt][K];
		for (int i = 0; i < cnt; i++)
			for (int j = 0; j < K; j++)
				embed[i][j] = 2 * (random.nextDouble()) / Math.sqrt(K);

		train = new LinkedList<Cai.TrainData>();

		for (int i = 0; i < data.getSizeA(); i++)
			for (int j : data.getLinksA(i)) {
				train.add(new TrainData(0, i, j, 1));
				train.add(new TrainData(0, i, random.nextInt(data.getSizeA()),
						0));
			}
		for (int i = 0; i < data.getSizeB(); i++)
			for (int j : data.getLinksB(i)) {
				train.add(new TrainData(1, i, j, 1));
				train.add(new TrainData(1, i, random.nextInt(data.getSizeB()),
						0));
			}

		double lacost = getCost();
		if (Config.getValue("DEBUG").equals("true"))
			System.out.println("Initial Cost = " + lacost);

		for (int r = 0; r < 100 && alpha > 1e-8; r++) {
			learn();
			double cost = getCost();
			if (cost < lacost)
				alpha *= 1.2;
			else
				alpha /= 2;
			lacost = cost;
			if (Config.getValue("DEBUG").equals("true"))
				System.out.println("Cost after Round " + r + " = " + cost
						+ ", alpha = " + alpha);
		}

		res = new HashMap<Integer, ArrayList<Integer>>();
		LinkedList<Integer> Q = new LinkedList<Integer>();
		for (int i = 0; i < data.getSizeA(); i++)
			Q.add(i);
		Thread[] workers = new Thread[Integer.valueOf(Config
				.getValue("Threads"))];
		for (int i = 0; i < workers.length; i++) {
			workers[i] = new Thread() {
				@Override
				public void run() {
					for (;;) {
						int id;
						synchronized (Q) {
							if (Q.isEmpty())
								return;
							id = Q.removeFirst();
						}
						ArrayList<Pair<Integer, Double>> cands = new ArrayList<Pair<Integer, Double>>();

						for (int j = 0; j < data.getSizeB(); j++)
							cands.add(new Pair<Integer, Double>(j, getScore(id,
									j)));

						Collections.sort(cands,
								new Comparator<Pair<Integer, Double>>() {
									public int compare(
											basic.format.Pair<Integer, Double> a,
											basic.format.Pair<Integer, Double> b) {
										return -a.getSecond().compareTo(
												b.getSecond());
									};
								});

						ArrayList<Integer> tres = new ArrayList<Integer>();
						for (Pair<Integer, Double> item : cands)
							if (tres.size() < MAXLIST)
								tres.add(item.getFirst());
							else
								break;
						synchronized (res) {
							res.put(id, tres);
						}
					}
				}
			};
			workers[i].start();
		}
		for (Thread worker : workers)
			try {
				worker.join();
			} catch (Exception e) {
			}
	}

	private void learn() {
		Collections.shuffle(train, random);
		// LinkedList<TrainData> Q = new LinkedList<Cai.TrainData>(train);
		Thread[] workers = new Thread[Integer.valueOf(Config
				.getValue("Threads"))];
		Iterator<TrainData> itr = train.iterator();

		for (int i = 0; i < workers.length; i++) {
			workers[i] = new Thread() {
				@Override
				public void run() {
					for (;;) {
						TrainData cur;
						synchronized (itr) {
							if (!itr.hasNext())
								return;
							cur = itr.next();
						}
						int ti, tj;
						if (cur.site == 0) {
							if (cur.v == 0) {
								ti = idA.get(random.nextInt(data.getSizeA()));
								tj = idA.get(random.nextInt(data.getSizeA()));
							} else {
								ti = idA.get(cur.i);
								tj = idA.get(cur.j);
							}
						} else {
							if (cur.v == 0) {
								ti = idB.get(random.nextInt(data.getSizeB()));
								tj = idB.get(random.nextInt(data.getSizeB()));
							} else {
								ti = idB.get(cur.i);
								tj = idB.get(cur.j);
							}
						}
						double curv = Functions.sigmoid(Vector.dot(embed[ti],
								embed[tj]));
						double err = curv - cur.v;
						double g = err * curv * (1 - curv);
						double[] la = new double[K];
						for (int k = 0; k < K; k++) {
							la[k] = embed[ti][k];
							embed[ti][k] += alpha
									* (-embed[tj][k] * g - lambda
											* embed[ti][k]);
						}
						for (int k = 0; k < K; k++)
							embed[tj][k] += alpha
									* (-la[k] * g - lambda * embed[tj][k]);
					}
				}
			};
			workers[i].start();
		}
		for (Thread worker : workers)
			try {
				worker.join();
			} catch (Exception e) {
			}
	}

	private double getCost() {
		double norm = 0;
		for (int i = 0; i < embed.length; i++)
			norm += Math.pow(Vector.norm(embed[i]), 2);
		double res = 0;
		for (TrainData row : train) {
			if (row.site == 0)
				res += Math.pow(
						Functions.sigmoid(Vector.dot(embed[idA.get(row.i)],
								embed[idA.get(row.j)])) - row.v, 2);
			else
				res += Math.pow(
						Functions.sigmoid(Vector.dot(embed[idB.get(row.i)],
								embed[idB.get(row.j)])) - row.v, 2);
		}
		return norm * lambda + res;
	}

	private double getScore(int i, int j) {
		// return Functions.sigmoid(Vector.dot(embed[idA.get(i)],
		// embed[idB.get(j)]));
		return Vector.CosineSimilarity(embed[idA.get(i)], embed[idB.get(j)]);
	}

	@Override
	public LinkedList<Pair<Integer, Integer>> getPairs() {
		LinkedList<Pair<Integer, Integer>> res = new LinkedList<Pair<Integer, Integer>>();
		HashMap<Integer, Integer> map = getMapping();
		for (int i : map.keySet())
			if (map.get(i) != -1)
				res.add(new Pair<Integer, Integer>(i, map.get(i)));
		return res;
	}

	@Override
	public HashMap<Integer, Integer> getMapping() {
		HashMap<Integer, Integer> map = new HashMap<Integer, Integer>();
		for (int i = 0; i < data.getSizeA(); i++) {
			int cur = -1;
			String curname = data.getNameA(i);
			for (int j = 0; cur == -1 && j < res.get(i).size() / 2; j++) {
				int k = res.get(i).get(j);
				if (curname.equals(data.getNameB(k)))
					cur = k;
			}
			for (int j = 0; cur == -1 && j < res.get(i).size() / 4; j++) {
				String nA = curname;
				int k = res.get(i).get(j);
				String nB = data.getNameB(k);
				if (nA.startsWith(nB) || nB.startsWith(nA) || nA.endsWith(nB)
						|| nB.endsWith(nA))
					cur = k;
			}
			for (int j = 0; cur == -1 && j < res.get(i).size() / 50; j++) {
				int k = res.get(i).get(j);
				String nB = data.getNameB(k);
				if (StringAlg.EditDistance(curname, nB) < 3)
					cur = k;
			}
			if (cur == -1)
				cur = res.get(i).get(0);
			map.put(i, cur);
		}
		return map;
	}

	@Override
	public HashMap<Integer, ArrayList<Integer>> getList() {
		return res;
	}

}
