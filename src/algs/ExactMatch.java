package algs;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;

import basic.format.Pair;

public class ExactMatch extends Model {

	private LinkedList<Pair<Integer, Integer>> pairs;

	@Override
	public void run() {
		pairs = new LinkedList<Pair<Integer, Integer>>();
		HashMap<String, Integer> Brid = new HashMap<String, Integer>();
		for (int i = 0; i < data.getSizeB(); i++)
			Brid.put(data.getNameB(i), i);
		for (int i = 0; i < data.getSizeA(); i++) {
			String nameA = data.getNameA(i);
			if (Brid.containsKey(nameA))
				pairs.add(new Pair<Integer, Integer>(i, Brid.get(nameA)));
		}
	}

	@Override
	public LinkedList<Pair<Integer, Integer>> getPairs() {
		return pairs;
	}

	@Override
	public HashMap<Integer, Integer> getMapping() {
		HashMap<Integer, Integer> res = new HashMap<Integer, Integer>();
		for (int i = 0; i < data.getSizeA(); i++)
			res.put(i, -1);
		for (Pair<Integer, Integer> link : pairs)
			res.put(link.getFirst(), link.getSecond());
		return res;
	}

	@Override
	public HashMap<Integer, ArrayList<Integer>> getList() {
		return null;
	}

}
