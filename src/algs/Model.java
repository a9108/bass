package algs;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;

import data.DataSet;
import basic.format.Pair;

public abstract class Model {
	protected DataSet data;

	protected String modelName;

	public abstract void run();

	public abstract LinkedList<Pair<Integer, Integer>> getPairs();

	public abstract HashMap<Integer, Integer> getMapping();

	public abstract HashMap<Integer, ArrayList<Integer>> getList();

	private void evaluate_Pairs() {
		try {
			LinkedList<Pair<Integer, Integer>> pairs = getPairs();
			int tot = 0;
			for (int i = 0; i < data.getSizeA(); i++)
				if (!data.getTrain().containsKey(i))
					if (data.getTruth(i) >= 0)
						tot++;
			int hit = 0, cnt = 0;
			for (Pair<Integer, Integer> link : pairs)
				if (!data.getTrain().containsKey(link.getFirst())) {
					if (data.getTruth(link.getFirst()) == link.getSecond())
						hit++;
					cnt++;
				}
			System.out.println("#############Evaluate Pairs###############");
			double p = hit / (cnt + 0.0);
			double r = hit / (tot + 0.0);
			System.out.println("Precision = " + p);
			System.out.println("Recall = " + r);
			System.out.println("F1-Score = " + 2 * p * r / (p + r));
			System.out.println("##########################################");
		} catch (Exception ex) {
		}
	}

	private void evaluate_Mapping() {
		try {
			HashMap<Integer, Integer> map = getMapping();
			int hit = 0, tot = data.getSizeA() - data.getTrain().size();
			for (int i : map.keySet())
				if (!data.getTrain().containsKey(i)) {
					if (data.getTruth(i) == map.get(i))
						hit++;
				}
			System.out.println("############Evaluate Mapping##############");
			double p = hit / (tot + 0.0);
			System.out.println("Precision = " + p);
			System.out.println("##########################################");
		} catch (Exception ex) {
		}
	}

	private void evaluate_List() {
		try {
			HashMap<Integer, ArrayList<Integer>> list = getList();
			if (list == null)
				return;
			System.out.println("#############Evaluate List################");
			int hit = 0, tot = data.getSizeA() - data.getTrain().size();
			for (int q = 0; q < 20; q++) {
				for (int i = 0; i < data.getSizeA(); i++)
					if (!data.getTrain().containsKey(i))
						try {
							if (list.get(i).get(q) == data.getTruth(i))
								hit++;
						} catch (Exception e) {
						}
				System.out.println("Precision @ " + q + " = " + hit
						/ (tot + 0.0));
			}
			System.out.println("##########################################");
		} catch (Exception e) {
		}

	}

	public void evaluate() {
		if (modelName == null)
			modelName = this.getClass().getName();
		System.out.println("=============" + modelName + "============");
		System.out.println("User Count : " + data.getSizeA());
		int tot = 0;
		for (int i = 0; i < data.getSizeA(); i++)
			if (data.getTruth(i) >= 0)
				tot++;
		System.out.println("Common User Count : " + tot);
		System.out.println("Training Size : " + data.getTrain().size());
		System.out.println("Test Size : " + (tot - data.getTrain().size()));
		evaluate_Pairs();
		evaluate_Mapping();
		evaluate_List();
	}

	public void runAll(DataSet data) {
		this.data = data;
		run();
		evaluate();
	}
}
