package algs;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

import javax.print.DocFlavor.STRING;

import org.apache.commons.collections.functors.ForClosure;
import org.apache.commons.lang.ArrayUtils;

import data.DataSet_WD;
import basic.Config;
import basic.StringOps;
import basic.Vector;
import basic.algorithm.Classification;
import basic.algorithm.StringAlg;
import basic.algorithm.WinSVM;
import basic.format.DenseFeature;
import basic.format.Feature;
import basic.format.Pair;

public class MOBIUS extends Model {

	private HashMap<Integer, Integer> curmap;
	private HashMap<Integer, HashSet<Integer>> cands;
	private HashMap<Integer, ArrayList<Pair<Double, Integer>>> list;
	private LinkedList<Feature> train, test;
	private LinkedList<Pair<Integer, Integer>> trainid, testid;
	private LinkedList<Pair<Double, Pair<Integer, Integer>>> olist;
	private Random random;
	private int nFeature = 9;

	private boolean isWD;
	private WeiDouTM mmtm;

	private Classification model;

	@Override
	public void run() {
		random = new Random();
		curmap = new HashMap<Integer, Integer>(data.getTrain());

		if (curmap.size() >= 0) {
			// System.out.println("####");
			HashMap<String, Integer> nameid = new HashMap<String, Integer>();
			for (int i = 0; i < data.getSizeB(); i++)
				nameid.put(data.getNameB(i), i);
			for (int i = 0; i < data.getSizeA(); i++)
				if (nameid.containsKey(data.getNameA(i)))
					curmap.put(i, nameid.get(data.getNameA(i)));
		}

		isWD = Config.getValue("DataSet").equals("WD");

		if (isWD) {
			mmtm = new WeiDouTM(data.getSizeA(), data.getSizeB(),
					((DataSet_WD) data).getMovieName().size(), 20, 1000);
			if (Config.getValue("UpdateMMTM").equals("true")) {
				mmtm.setMoviename(((DataSet_WD) data).getMovieName());
				for (int i = 0; i < data.getSizeA(); i++)
					mmtm.addLink(i, data.getTruth(i));
				for (int i = 0; i < data.getSizeA(); i++)
					for (int mid : ((DataSet_WD) data).getMovies(i))
						mmtm.addMovie(i, mid);
				for (int i = 0; i < data.getSizeB(); i++)
					for (String s : ((DataSet_WD) data).getWeibo(i))
						mmtm.addWeibo(i, s);
				mmtm.learn();
			} else
				mmtm.load(Config.getValue("WDDir"));
		}

		genCands();
		genTrain();
		genTest();

		learnModel();

		genMapping();
	}

	private void genCands() {
		cands = new HashMap<Integer, HashSet<Integer>>();
		for (int i = 0; i < data.getSizeA(); i++) {
			HashSet<Integer> candi = new HashSet<Integer>();
			HashSet<Integer> Q = data.getLinksA(i);
			Q.add(i);
			for (int j : Q)
				if (curmap.containsKey(j) && curmap.get(j) != -1) {
					int k = curmap.get(j);
					candi.add(k);
					candi.addAll(data.getLinksB(k));
				}
			cands.put(i, candi);
		}
	}

	private Feature genFeature(int i, int j, int v) {
		Feature f = new DenseFeature();
		f.setSize(nFeature);
		f.setResult(v);
		String na = data.getNameA(i);
		String nb = data.getNameB(j);
		int dist = StringAlg.EditDistance(na, nb);
		int id = 0;
		f.setValue(id++, dist);
		f.setValue(id++, dist / (0. + na.length() + nb.length()));
		f.setValue(id++, dist / (0. + Math.max(na.length(), nb.length())));
		f.setValue(id++, (na.contains(nb) || nb.contains(na)) ? 1 : 0);
		f.setValue(id++, (na.equals(nb)) ? 1 : 0);
		f.setValue(id++, na.length());
		f.setValue(id++, nb.length());
		return f;
	}

	private void genTrain() {
		train = new LinkedList<Feature>();
		trainid = new LinkedList<Pair<Integer, Integer>>();
		HashMap<Integer, Integer> basemap = new HashMap<Integer, Integer>(
				data.getTrain());
		basemap = curmap;

		LinkedList<Pair<Integer, Integer>> Q = new LinkedList<Pair<Integer, Integer>>();
		for (int i : basemap.keySet())
			if (basemap.get(i) != -1)
				Q.add(new Pair<Integer, Integer>(i, basemap.get(i)));

		Thread[] workers = new Thread[Integer.valueOf(Config
				.getValue("Threads"))];
		for (int i = 0; i < workers.length; i++) {
			workers[i] = new Thread() {
				@Override
				public void run() {
					for (;;) {
						int i, j;
						synchronized (Q) {
							if (Q.isEmpty())
								return;
							i = Q.getFirst().getFirst();
							j = Q.getFirst().getSecond();
							Q.removeFirst();
						}
						Feature f = genFeature(i, j, 1);
						synchronized (trainid) {
							trainid.add(new Pair<Integer, Integer>(i, j));
							train.add(f);
						}

						int left = 5;
						if (list != null && list.containsKey(i))
							for (int k = 0; k < list.get(i).size() && left > 0; k++) {
								int l = list.get(i).get(k).getSecond();
								if (l == j)
									continue;
								left--;
								f = genFeature(i, l, 0);
								synchronized (trainid) {
									trainid.add(new Pair<Integer, Integer>(i, l));
									train.add(f);
								}

							}
						left += 5;
						for (int l : cands.get(i))
							if (left == 0)
								break;
							else if (l != j) {
								left--;
								f = genFeature(i, l, 0);
								synchronized (trainid) {
									trainid.add(new Pair<Integer, Integer>(i, l));
									train.add(f);
								}
							}

					}
				}
			};
			workers[i].start();
		}
		for (Thread worker : workers)
			try {
				worker.join();
			} catch (Exception e) {
			}
		System.out.println("Training Size = " + trainid.size());
	}

	private void genTest() {
		test = new LinkedList<Feature>();
		testid = new LinkedList<Pair<Integer, Integer>>();
		LinkedList<Integer> Q = new LinkedList<Integer>(cands.keySet());
		Thread[] workers = new Thread[Integer.valueOf(Config
				.getValue("Threads"))];
		for (int i = 0; i < workers.length; i++) {
			workers[i] = new Thread() {
				@Override
				public void run() {
					for (;;) {
						int i;
						synchronized (Q) {
							if (Q.isEmpty())
								return;
							i = Q.removeFirst();
						}
						for (int j : cands.get(i)) {
							Feature f = genFeature(i, j, 0);
							synchronized (testid) {
								testid.add(new Pair<Integer, Integer>(i, j));
								test.add(f);
							}
						}
					}
				}
			};
			workers[i].start();
		}
		for (Thread worker : workers)
			try {
				worker.join();
			} catch (Exception e) {
			}
		System.out.println("Test Size = " + test.size());
	}

	private void learnModel() {

		String cmd = "-t 0 -h 0 -b 1";
		if (!Config.getValue("DEBUG").equals("true"))
			cmd = cmd + " -q";
		model = new WinSVM(Config.getValue("SVMDIR"), cmd, "-b 1");
		model.setNFeature(nFeature);
		for (Feature f : train)
			model.addTrain(f);
		model.train();
		ArrayList<Double> res = model.predict(test);
		model.destroy();
		Iterator<Double> ires = res.iterator();
		Iterator<Pair<Integer, Integer>> iid = testid.iterator();
		list = new HashMap<Integer, ArrayList<Pair<Double, Integer>>>();
		for (; ires.hasNext() && iid.hasNext();) {
			Pair<Integer, Integer> id = iid.next();
			int i = id.getFirst();
			int j = id.getSecond();
			double v = ires.next();
			if (!list.containsKey(i))
				list.put(i, new ArrayList<Pair<Double, Integer>>());
			list.get(i).add(new Pair<Double, Integer>(v, j));
		}
		for (int i : list.keySet()) {
			Collections.sort(list.get(i));
			Collections.reverse(list.get(i));
		}
	}

	private void genMapping() {
		curmap = new HashMap<Integer, Integer>(data.getTrain());
		if (Config.getValue("MappingAlg").equals("Top-1")) {
			for (int i = 0; i < data.getSizeA(); i++)
				if (list.containsKey(i))
					curmap.put(i, list.get(i).get(0).getSecond());
			return;
		}
		HashSet<Integer> usedB = new HashSet<Integer>(curmap.values());
		olist = new LinkedList<Pair<Double, Pair<Integer, Integer>>>();
		for (int i : list.keySet())
			for (Pair<Double, Integer> link : list.get(i))
				olist.add(new Pair<Double, Pair<Integer, Integer>>(link
						.getFirst(), new Pair<Integer, Integer>(i, link
						.getSecond())));
		Collections.sort(olist);
		Collections.reverse(olist);
		int cnt = 0;
		for (Pair<Double, Pair<Integer, Integer>> link : olist) {
			cnt++;
			if (cnt > olist.size() / 20)
				break;
			int i = link.getSecond().getFirst();
			int j = link.getSecond().getSecond();
			if (curmap.containsKey(i) || usedB.contains(j))
				continue;
			curmap.put(i, j);
			usedB.add(j);
		}
		for (int i = 0; i < data.getSizeA(); i++)
			if (!curmap.containsKey(i))
				curmap.put(i, -1);
	}

	@Override
	public LinkedList<Pair<Integer, Integer>> getPairs() {
		// int SIZE = data.getSizeA() - data.getTrain().size();
		// SIZE *= 1.5;
		// LinkedList<Pair<Integer, Integer>> pairs = new
		// LinkedList<Pair<Integer, Integer>>();
		// for (Pair<Double, Pair<Integer, Integer>> link : olist)
		// if (pairs.size() == SIZE)
		// break;
		// else if (!data.getTrain().containsKey(link.getSecond().getFirst()))
		// pairs.add(link.getSecond());
		// return pairs;
		LinkedList<Pair<Integer, Integer>> res = new LinkedList<Pair<Integer, Integer>>();
		HashMap<Integer, Integer> map = getMapping();
		for (int i : map.keySet())
			if (map.get(i) != -1)
				res.add(new Pair<Integer, Integer>(i, map.get(i)));
		return res;
	}

	@Override
	public HashMap<Integer, Integer> getMapping() {
		return curmap;
	}

	@Override
	public HashMap<Integer, ArrayList<Integer>> getList() {
		HashMap<Integer, ArrayList<Integer>> plist = new HashMap<Integer, ArrayList<Integer>>();
		for (int i : list.keySet()) {
			ArrayList<Integer> curlist = new ArrayList<Integer>();
			for (Pair<Double, Integer> item : list.get(i))
				curlist.add(item.getSecond());
			plist.put(i, curlist);
		}
		return plist;
	}
}
