package data;

import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Random;

import basic.Config;
import basic.FileOps;

public abstract class DataSet {
	public abstract void genData();

	public abstract void load();

	public abstract int getSizeA();

	public abstract int getSizeB();

	public abstract String getNameA(int i);

	public abstract String getNameB(int i);

	public abstract HashSet<Integer> getLinksA(int i);

	public abstract HashSet<Integer> getLinksB(int i);

	public abstract void genTrain(Random random, double ratio);

	public abstract HashMap<Integer, Integer> getTrain();

	public abstract int getTruth(int i);
}
