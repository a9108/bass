package data;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Random;

import basic.Config;
import basic.FileOps;

public class DataSet_WD extends DataSet {
	private HashMap<String, Integer> did, wid;

	private Network dlink, wlink;
	private ArrayList<String> dname, wname;
	private ArrayList<HashSet<Integer>> movies;
	private ArrayList<LinkedList<String>> weibos;
	private HashMap<Integer, String> moviename;

	@Override
	public void genData() {
		System.out.println("####");
		did = new HashMap<String, Integer>();
		wid = new HashMap<String, Integer>();
		int cnt = 0;
		for (String line : FileOps.LoadFilebyLine(Config.getValue("WeiDouDir")
				+ "users")) {
			String[] sep = line.split("\t");
			if (sep.length != 3)
				continue;
			if (wid.containsKey(sep[2]))
				continue;
			did.put(sep[1], cnt);
			wid.put(sep[2], cnt++);
		}
		dname = new ArrayList<String>();
		for (int i = 0; i < did.size(); i++)
			dname.add("");
		for (String line : FileOps.LoadFilebyLine(Config.getValue("WeiDouDir")
				+ "douban_username")) {
			String[] sep = line.split("\t");
			if (sep.length == 2)
				if (did.containsKey(sep[0]))
					dname.set(did.get(sep[0]), sep[1]);
		}
		wname = new ArrayList<String>();
		for (int i = 0; i < wid.size(); i++)
			wname.add("");
		for (String line : FileOps.LoadFilebyLine(Config.getValue("WeiDouDir")
				+ "weibo_username")) {
			String[] sep = line.split("\t");
			if (sep.length == 2)
				if (wid.containsKey(sep[0]))
					wname.set(wid.get(sep[0]), sep[1]);
		}
		dlink = new Network();
		for (String line : FileOps.LoadFilebyLine(Config.getValue("WeiDouDir")
				+ "douban_friend")) {
			String[] sep = line.split("\t");
			if (sep.length != 2)
				continue;
			if (did.containsKey(sep[0]) && did.containsKey(sep[1]))
				dlink.insert(did.get(sep[0]), did.get(sep[1]));
		}
		wlink = new Network();
		for (String line : FileOps.LoadFilebyLine(Config.getValue("WeiDouDir")
				+ "weibo_friend")) {
			String[] sep = line.split("\t");
			if (sep.length != 2)
				continue;
			if (wid.containsKey(sep[0]) && wid.containsKey(sep[1]))
				wlink.insert(wid.get(sep[0]), wid.get(sep[1]));
		}

		LinkedList<Integer> rmq = new LinkedList<Integer>();
		HashSet<Integer> active = new HashSet<Integer>();
		for (int i = 0; i < did.size(); i++)
			active.add(i);

		System.out.println("# Initial Active User : " + active.size());

		wlink.retainAll(active);
		dlink.retainAll(active);

		int thres = Integer.valueOf(Config.getValue("DegreeThres"));
		for (int i : active)
			if (wlink.getDegree(i) < thres || dlink.getDegree(i) < thres)
				rmq.add(i);
		for (; rmq.size() > 0;) {
			int id = rmq.removeFirst();
			if (!active.contains(id))
				continue;
			active.remove(id);
			for (int ti : wlink.getLinks(id))
				if (wlink.getDegree(ti) <= thres)
					rmq.add(ti);
			for (int ti : dlink.getLinks(id))
				if (dlink.getDegree(ti) <= thres)
					rmq.add(ti);
			wlink.remove(id);
			dlink.remove(id);
		}

		System.out.println("# Remained Active User : " + active.size());

		HashMap<Integer, Integer> rid = new HashMap<Integer, Integer>();
		LinkedList<Integer> oid = new LinkedList<Integer>();
		for (int id : active) {
			rid.put(id, rid.size());
			oid.add(id);
		}
		LinkedList<String> sdname = new LinkedList<String>();
		LinkedList<String> swname = new LinkedList<String>();
		for (int id : oid) {
			sdname.add(dname.get(id));
			swname.add(wname.get(id));
		}
		FileOps.SaveFile(Config.getValue("WDDir") + "douban_name", sdname);
		FileOps.SaveFile(Config.getValue("WDDir") + "weibo_name", swname);
		LinkedList<String> sdlink = new LinkedList<String>();
		LinkedList<String> swlink = new LinkedList<String>();
		int cur = 0;
		for (int id : oid) {
			for (int j : dlink.getLinks(id))
				sdlink.add(cur + "\t" + rid.get(j));
			for (int j : wlink.getLinks(id))
				swlink.add(cur + "\t" + rid.get(j));
			cur++;
		}
		FileOps.SaveFile(Config.getValue("WDDir") + "douban_link", sdlink);
		FileOps.SaveFile(Config.getValue("WDDir") + "weibo_link", swlink);

		HashMap<Integer, LinkedList<Integer>> movies = new HashMap<Integer, LinkedList<Integer>>();
		for (String s : FileOps.LoadFilebyLine(Config.getValue("WeiDouDir")
				+ "douban_userwatched")) {
			String[] sep = s.split("\t");
			if (sep.length != 3)
				continue;
			if (!movies.containsKey(did.get(sep[0])))
				movies.put(did.get(sep[0]), new LinkedList<Integer>());
			movies.get(did.get(sep[0])).add(Integer.valueOf(sep[1]));
		}
		LinkedList<String> smovies = new LinkedList<String>();
		for (int id : oid) {
			if (!movies.containsKey(id))
				smovies.add("");
			else {
				StringBuilder sb = new StringBuilder();
				for (int mid : movies.get(id)) {
					if (sb.length() > 0)
						sb.append("\t");
					sb.append(mid);
				}
				smovies.add(sb.toString());
			}
		}
		FileOps.SaveFile(Config.getValue("WDDir") + "douban_watched", smovies);

		HashMap<Integer, LinkedList<String>> weibos = new HashMap<Integer, LinkedList<String>>();
		for (String s : FileOps.LoadFilebyLine(Config.getValue("WeiDouDir")
				+ "weibo_weibo")) {
			int pos = s.indexOf(':');
			if (pos >= s.length())
				continue;
			String id = s.substring(0, pos);
			String content = s.substring(pos + 1);
			int tid = wid.get(id);
			if (!weibos.containsKey(tid))
				weibos.put(tid, new LinkedList<String>());
			weibos.get(tid).add(content);
		}
		LinkedList<String> sweibo = new LinkedList<String>();
		for (int id : oid)
			if (weibos.containsKey(id)) {
				for (String content : weibos.get(id))
					sweibo.add(rid.get(id) + ":" + content);
			}
		FileOps.SaveFile(Config.getValue("WDDir") + "weibo_weibo", sweibo);
	}

	@Override
	public void load() {
		dname = new ArrayList<String>(FileOps.LoadFilebyLine(Config
				.getValue("WDDir") + "douban_name"));
		wname = new ArrayList<String>(FileOps.LoadFilebyLine(Config
				.getValue("WDDir") + "weibo_name"));
		dlink = new Network(Config.getValue("WDDir") + "douban_link");
		wlink = new Network(Config.getValue("WDDir") + "weibo_link");

		movies = new ArrayList<HashSet<Integer>>();
		for (String s : FileOps.LoadFilebyLine(Config.getValue("WDDir")
				+ "douban_watched")) {
			HashSet<Integer> cur = new HashSet<Integer>();
			for (String ts : s.split("\t"))
				cur.add(Integer.valueOf(ts));
			movies.add(cur);
		}

		weibos = new ArrayList<LinkedList<String>>();
		for (String s : FileOps.LoadFilebyLine(Config.getValue("WDDir")
				+ "weibo_weibo")) {
			int pos = s.indexOf(':');
			if (pos >= s.length())
				continue;
			int id = Integer.valueOf(s.substring(0, pos));
			for (; weibos.size() <= id;)
				weibos.add(new LinkedList<String>());
			weibos.get(id).add(s.substring(pos + 1));
		}

		moviename = new HashMap<Integer, String>();
		for (String s : FileOps.LoadFilebyLine(Config.getValue("WeiDouDir")
				+ "douban_movie")) {
			String[] sep = s.split("\t");
			if (sep.length < 3)
				continue;
			moviename.put(Integer.valueOf(sep[0]), sep[2]);
		}
	}

	public HashMap<Integer, String> getMovieName() {
		return moviename;
	}

	public HashSet<Integer> getMovies(int i) {
		return movies.get(i);
	}

	public LinkedList<String> getWeibo(int i) {
		return weibos.get(i);
	}

	@Override
	public int getSizeA() {
		return dname.size();
	}

	@Override
	public int getSizeB() {
		return wname.size();
	}

	@Override
	public String getNameA(int i) {
		return dname.get(i);
	}

	@Override
	public String getNameB(int i) {
		return wname.get(i);
	}

	@Override
	public HashSet<Integer> getLinksA(int i) {
		return new HashSet<Integer>(dlink.getLinks(i));
	}

	@Override
	public HashSet<Integer> getLinksB(int i) {
		return new HashSet<Integer>(wlink.getLinks(i));
	}

	private HashMap<Integer, Integer> train;

	@Override
	public void genTrain(Random random, double ratio) {
		train = new HashMap<Integer, Integer>();
		for (int i = 0; i < getSizeA(); i++)
			if (random.nextDouble() < ratio)
				train.put(i, i);
	}

	@Override
	public HashMap<Integer, Integer> getTrain() {
		return train;
	}

	@Override
	public int getTruth(int i) {
		return i;
	}

}
