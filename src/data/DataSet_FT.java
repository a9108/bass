package data;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Random;

import basic.Config;
import basic.FileOps;

public class DataSet_FT extends DataSet {

	private ArrayList<String> fname, tname;
	private Network flink, tlink;

	private HashMap<Integer, Integer> train;

	@Override
	public void genData() {
		Network facebook = new Network(Config.getValue("AboutMeDir")
				+ "networks\\facebookfriends");
		System.out.println("# Facebook Links : " + facebook.size());
		Network twitter = new Network(Config.getValue("AboutMeDir")
				+ "networks\\twitterfriends");
		System.out.println("# Twitter Links : " + twitter.size());
		HashMap<Integer, String> fname = FileOps.LoadDictionIS(Config
				.getValue("AboutMeDir") + "id2fname");
		System.out.println("# Facebook User : " + fname.size());
		HashMap<Integer, String> tname = FileOps.LoadDictionIS(Config
				.getValue("AboutMeDir") + "id2tname");
		System.out.println("# Twitter User : " + tname.size());

		LinkedList<Integer> rmq = new LinkedList<Integer>();
		HashSet<Integer> active = new HashSet<Integer>();
		active.addAll(fname.keySet());
		active.retainAll(tname.keySet());

		System.out.println("# Initial Active User : " + active.size());

		facebook.retainAll(active);
		twitter.retainAll(active);

		int thres = Integer.valueOf(Config.getValue("DegreeThres"));
		for (int i : active)
			if (facebook.getDegree(i) < thres || twitter.getDegree(i) < thres)
				rmq.add(i);
		for (; rmq.size() > 0;) {
			int id = rmq.removeFirst();
			if (!active.contains(id))
				continue;
			active.remove(id);
			for (int ti : facebook.getLinks(id))
				if (facebook.getDegree(ti) <= thres)
					rmq.add(ti);
			for (int ti : twitter.getLinks(id))
				if (twitter.getDegree(ti) <= thres)
					rmq.add(ti);
			facebook.remove(id);
			twitter.remove(id);
		}

		System.out.println("# Remained Active User : " + active.size());

		HashMap<Integer, Integer> rid = new HashMap<Integer, Integer>();
		LinkedList<Integer> oid = new LinkedList<Integer>();
		for (int id : active) {
			rid.put(id, rid.size());
			oid.add(id);
		}
		LinkedList<String> sfname = new LinkedList<String>();
		LinkedList<String> stname = new LinkedList<String>();
		for (int id : oid) {
			sfname.add(simplifyName(fname.get(id)));
			stname.add(simplifyName(tname.get(id)));
		}
		FileOps.SaveFile(Config.getValue("FTDir") + "fname", sfname);
		FileOps.SaveFile(Config.getValue("FTDir") + "tname", stname);
		LinkedList<String> flink = new LinkedList<String>();
		LinkedList<String> tlink = new LinkedList<String>();
		int cur = 0;
		for (int id : oid) {
			for (int j : facebook.getLinks(id))
				flink.add(cur + "\t" + rid.get(j));
			for (int j : twitter.getLinks(id))
				tlink.add(cur + "\t" + rid.get(j));
			cur++;
		}
		FileOps.SaveFile(Config.getValue("FTDir") + "flink", flink);
		FileOps.SaveFile(Config.getValue("FTDir") + "tlink", tlink);
	}

	@Override
	public void load() {
		fname = new ArrayList<String>(FileOps.LoadFilebyLine(Config
				.getValue("FTDir") + "fname"));
		tname = new ArrayList<String>(FileOps.LoadFilebyLine(Config
				.getValue("FTDir") + "tname"));
		flink = new Network(Config.getValue("FTDir") + "flink");
		tlink = new Network(Config.getValue("FTDir") + "tlink");
	}

	private String simplifyName(String s) {
		return s.toLowerCase().replace(" ", "").replace("-", "")
				.replace("_", "");
	}

	@Override
	public String getNameA(int i) {
		return fname.get(i);
	}

	@Override
	public String getNameB(int i) {
		return tname.get(i);
	}

	@Override
	public HashSet<Integer> getLinksA(int i) {
		return new HashSet<Integer>(flink.getLinks(i));
	}

	@Override
	public HashSet<Integer> getLinksB(int i) {
		return new HashSet<Integer>(tlink.getLinks(i));
	}

	@Override
	public int getSizeA() {
		return fname.size();
	}

	@Override
	public int getSizeB() {
		return tname.size();
	}

	@Override
	public int getTruth(int i) {
		return i;
	}

	@Override
	public void genTrain(Random random, double ratio) {
		train = new HashMap<Integer, Integer>();
		for (int i = 0; i < getSizeA(); i++)
			if (random.nextDouble() < ratio)
				train.put(i, i);
	}

	@Override
	public HashMap<Integer, Integer> getTrain() {
		return new HashMap<Integer, Integer>(train);
	}
}
