package data;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Random;

import javax.print.attribute.standard.Fidelity;

import basic.Config;
import basic.FileOps;
import basic.format.Pair;

public class DataSet_FTP extends DataSet {

	private ArrayList<String> fname, tname;
	private Network flink, tlink;
	private HashMap<Integer, Integer> truth;

	private HashMap<Integer, Integer> train;

	@Override
	public void genData() {
		Network facebook = new Network(Config.getValue("AboutMeDir")
				+ "networks\\facebookfriends");
		System.out.println("# Facebook Links : " + facebook.size());
		Network twitter = new Network(Config.getValue("AboutMeDir")
				+ "networks\\twitterfriends");
		System.out.println("# Twitter Links : " + twitter.size());
		HashMap<Integer, String> fname = FileOps.LoadDictionIS(Config
				.getValue("AboutMeDir") + "id2fname");
		System.out.println("# Facebook User : " + fname.size());
		HashMap<Integer, String> tname = FileOps.LoadDictionIS(Config
				.getValue("AboutMeDir") + "id2tname");
		System.out.println("# Twitter User : " + tname.size());

		LinkedList<Integer> rmq = new LinkedList<Integer>();
		HashSet<Integer> active = new HashSet<Integer>();
		LinkedList<Integer> allids = new LinkedList<Integer>();
		allids.addAll(fname.keySet());
		allids.addAll(tname.keySet());

		facebook.retainAll(fname.keySet());
		twitter.retainAll(tname.keySet());

		Network ofacebook = new Network(facebook);
		Network otwitter = new Network(twitter);

		active.addAll(fname.keySet());
		active.retainAll(tname.keySet());
		facebook.retainAll(active);
		twitter.retainAll(active);

		System.out.println("# Initial Active User : " + active.size());

		int thres = Integer.valueOf(Config.getValue("DegreeThres"));

		// HashSet<Integer> inq = new HashSet<Integer>();
		// for (int i : active)
		// if (facebook.getDegree(i) < thres && twitter.getDegree(i) < thres) {
		// rmq.add(i);
		// inq.add(i);
		// }
		// for (; rmq.size() > 0;) {
		// int id = rmq.removeFirst();
		// if (active.contains(id)) {
		// active.remove(id);
		// for (int ti : facebook.getLinks(id))
		// if (facebook.getDegree(ti) <= thres
		// && twitter.getDegree(ti) < thres)
		// if (!inq.contains(ti)) {
		// rmq.add(ti);
		// inq.add(ti);
		// }
		// for (int ti : twitter.getLinks(id))
		// if (facebook.getDegree(ti) < thres
		// && twitter.getDegree(ti) <= thres)
		// if (!inq.contains(ti)) {
		// rmq.add(ti);
		// inq.add(ti);
		// }
		// facebook.remove(id);
		// twitter.remove(id);
		// }
		// if (rmq.size() == 0)
		// for (int i : active)
		// if (active.size() - rmq.size() > 10000)
		// if (facebook.getDegree(i) < thres
		// || twitter.getDegree(i) < thres) {
		// if (!inq.contains(i)) {
		// rmq.add(i);
		// inq.add(i);
		// }
		// }
		// }

		for (int i : active)
			if (facebook.getDegree(i) < thres || twitter.getDegree(i) < thres)
				rmq.add(i);
		for (; rmq.size() > 0;) {
			int id = rmq.removeFirst();
			if (!active.contains(id))
				continue;
			active.remove(id);
			for (int ti : facebook.getLinks(id))
				if (facebook.getDegree(ti) <= thres)
					rmq.add(ti);
			for (int ti : twitter.getLinks(id))
				if (twitter.getDegree(ti) <= thres)
					rmq.add(ti);
			facebook.remove(id);
			twitter.remove(id);
		}

		System.out.println("# Remained Active User : " + active.size());

		int cnt = (int) (active.size() / (1. - Double.valueOf(Config
				.getDouble("NonMatchRatio"))));

		System.out.println(" Target User Size : " + cnt);

		allids.removeAll(active);
		allids.removeAll(tname.keySet());
		Collections.shuffle(allids, new Random(0));
		Thread[] workers = new Thread[Config.getInt("Threads")];
		for (int i = 0; i < workers.length; i++) {
			workers[i] = new Thread() {
				public void run() {
					for (;;) {
						int i;
						synchronized (allids) {
							if (allids.isEmpty())
								return;
							i = allids.removeFirst();
						}
						// HashSet<Integer> flinks;
						// synchronized (active) {
						// flinks = new HashSet<Integer>(active);
						// }
						// flinks.retainAll(ofacebook.getLinks(i));
						// if (flinks.size() >= thres)
						synchronized (active) {
							if (active.size() >= cnt)
								return;
							active.add(i);
						}
					}
				};
			};
			workers[i].start();
		}
		for (Thread worker : workers)
			try {
				worker.join();
			} catch (Exception e) {
			}
		// for (int i : allids)
		// if (active.size() < cnt) {
		// HashSet<Integer> flinks = new HashSet<Integer>(active);
		// flinks.retainAll(ofacebook.getLinks(i));
		// if (flinks.size() >= thres) {
		// active.add(i);
		// }
		// }

		System.out.println("# Final Active User : " + active.size());
		ofacebook.retainAll(active);
		otwitter.retainAll(active);

		HashMap<Integer, Integer> frid = new HashMap<Integer, Integer>();
		HashMap<Integer, Integer> trid = new HashMap<Integer, Integer>();
		LinkedList<String> sfname = new LinkedList<String>();
		LinkedList<String> stname = new LinkedList<String>();
		LinkedList<Pair<Integer, Integer>> truth = new LinkedList<Pair<Integer, Integer>>();
		for (int id : active) {
			if (fname.containsKey(id)) {
				frid.put(id, frid.size());
				sfname.add(simplifyName(fname.get(id)));
			}
			if (tname.containsKey(id)) {
				trid.put(id, trid.size());
				stname.add(simplifyName(tname.get(id)));
			}
			if (frid.containsKey(id) && trid.containsKey(id))
				truth.add(new Pair<Integer, Integer>(frid.get(id), trid.get(id)));
		}

		System.out.println("# Links : " + truth.size());

		FileOps.SaveFile(Config.getValue("FTPDir") + "fname", sfname);
		FileOps.SaveFile(Config.getValue("FTPDir") + "tname", stname);
		FileOps.SaveList(Config.getValue("FTPDir") + "truth", truth);

		LinkedList<String> flink = new LinkedList<String>();
		LinkedList<String> tlink = new LinkedList<String>();
		for (int i : ofacebook.getNodes())
			for (int j : ofacebook.getLinks(i))
				flink.add(frid.get(i) + "\t" + frid.get(j));
		for (int i : otwitter.getNodes())
			for (int j : otwitter.getLinks(i))
				tlink.add(trid.get(i) + "\t" + trid.get(j));
		FileOps.SaveFile(Config.getValue("FTPDir") + "flink", flink);
		FileOps.SaveFile(Config.getValue("FTPDir") + "tlink", tlink);
	}

	@Override
	public void load() {
		fname = new ArrayList<String>(FileOps.LoadFilebyLine(Config
				.getValue("FTPDir") + "fname"));
		tname = new ArrayList<String>(FileOps.LoadFilebyLine(Config
				.getValue("FTPDir") + "tname"));
		flink = new Network(Config.getValue("FTPDir") + "flink");
		tlink = new Network(Config.getValue("FTPDir") + "tlink");
		truth = FileOps.LoadDictionII(Config.getValue("FTPDir") + "truth");
	}

	private String simplifyName(String s) {
		return s.toLowerCase().replace(" ", "").replace("-", "")
				.replace("_", "");
	}

	@Override
	public String getNameA(int i) {
		return fname.get(i);
	}

	@Override
	public String getNameB(int i) {
		return tname.get(i);
	}

	@Override
	public HashSet<Integer> getLinksA(int i) {
		return new HashSet<Integer>(flink.getLinks(i));
	}

	@Override
	public HashSet<Integer> getLinksB(int i) {
		return new HashSet<Integer>(tlink.getLinks(i));
	}

	@Override
	public int getSizeA() {
		return fname.size();
	}

	@Override
	public int getSizeB() {
		return tname.size();
	}

	@Override
	public int getTruth(int i) {
		if (truth.containsKey(i))
			return truth.get(i);
		return -1;
	}

	@Override
	public void genTrain(Random random, double ratio) {
		train = new HashMap<Integer, Integer>();
		for (int i : truth.keySet())
			if (random.nextDouble() < ratio)
				train.put(i, truth.get(i));
	}

	@Override
	public HashMap<Integer, Integer> getTrain() {
		return new HashMap<Integer, Integer>(train);
	}
}
