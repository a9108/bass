package exp;

import java.util.Random;

import data.DataSet;
import algs.BASS;
import algs.Cai;
import algs.EditDistance;
import algs.ExactMatch;
import algs.MOBIUS;
import basic.Config;

public class Entry {
	public static void main(String[] args) {
		Config.load("Config.txt");

		DataSet data;
		try {
			data = (DataSet) Class.forName(
					"data.DataSet_" + Config.getValue("DataSet")).newInstance();
		} catch (Exception e) {
			System.out.println("UNKNOWN DATASET");
			return;
		}

//		for (double ratio = 0; ratio < 0.6; ratio += 0.1) {
//			Config.setValue("NonMatchRatio", "" + ratio);
			if (Config.getValue("GenerateData").equals("true"))
				data.genData();

			data.load();
			Random random = new Random(0);

			data.genTrain(random, Double.valueOf(Config.getValue("TrainRatio")));

//			 new ExactMatch().runAll(data);
			// new Cai().runAll(data);
			 new BASS().runAll(data);
//			new MOBIUS().runAll(data);
//		}
	}
}
